package com.emids.health;

import java.util.Scanner;

public class Health_Insurance {

	
	
	public static void main(String[] args) {
		try{
		Scanner sc=new Scanner(System.in);  
	     
		/*Name: Norman Gomes
		Gender: Male
		Age: 34 years
		Current health:


		Hypertension: No
		Blood pressure: No
		Blood sugar: No
		Overweight: Yes


		Habits:


		Smoking: No
		Alcohol: Yes
		Daily exercise: Yes
		Drugs: No*/
		   
		   System.out.println("Enter your Name :");  
		   String name=sc.next();
		   
		   System.out.println("Enter your Gender (male/female) :");  
		   String gender=sc.next(); 
		   
		   System.out.println("Enter your Age (in years) :");  
		   int age=sc.nextInt();  
		   
		   System.out.println("Current Health : ");
		   System.out.println("Hypertension  (yes/no) :");
		   String hypertension=sc.next();
		   System.out.println("Blood pressure  (yes/no) :");
		   String blood_pressure=sc.next();
		   System.out.println("Blood Sugar  (yes/no) :");
		   String blood_sugar=sc.next();
		   System.out.println("Overweight  (yes/no) :");
		   String overweight=sc.next();
		   
		   
		   
		   System.out.println("Habbits : ");
		   System.out.println("Smoking  (yes/no) :");
		   String smoking=sc.next();
		   System.out.println("Alcohol  (yes/no) :");
		   String alcohol=sc.next();
		   System.out.println("Daily Exercise  (yes/no) :");
		   String daily_exercise=sc.next();
		   System.out.println("Drugs  (yes/no) :");
		   String drugs=sc.next();
		   
		   
		  /* OUTPUT


		   Health Insurance Premium for Mr. Gomes: Rs. 6,856



		   BUSINESS RULES


		   Base premium for anyone below the age of 18 years = Rs. 5,000
		   % increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% increase every 5 years
		   Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for Males
		   Pre-existing conditions (Hypertension | Blook pressure | Blood sugar | Overweight) -> Increase of 1% per condition
		   Habits


		   Good habits (Daily exercise) -> Reduce 3% for every good habit
		   Bad habits (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit*/
		   
		   int output=0;
		   double premium=5000;
		   double base=5000;
		   String title="";
		   
		   if(gender!=null){
			   if(gender.equalsIgnoreCase("male")){
				   title="Mr";
			   }
			   else{
				   title="Ms";
			   }
		   }
		   
		   if(age!=0){
			   //age 
			   if(age<=18){
				   premium=base;
			   }
			   else if(age>18 && age<=40){
				   
				   premium=premium+(premium*0.1); 
				   
				   //System.out.println("age "+premium);
			   }
			   else if(age>40){
				   int personAge = age - 40;
			        while (personAge >= 5) {
			            premium = premium+(premium*0.2);
			            personAge -= 5;
			        }
			   }
		   } 
			   //gender
			   if(!gender.equalsIgnoreCase("male")){
				   premium=premium+(premium*0.02); //System.out.println("gender"+ premium);
			   }
			   
			   //health
		
				   
				   if(hypertension.equalsIgnoreCase("yes")){
					   premium=premium+(premium*0.01); //System.out.println("hyper"+ premium);
				   }
				   if(blood_pressure.equalsIgnoreCase("yes")){
					   premium=premium+(premium*0.01); //System.out.println("pressure"+ premium);
				   }
				   if(blood_sugar.equalsIgnoreCase("yes")){
					   premium=premium+(premium*0.01); //System.out.println("sugar"+ premium);
				   }
				   if(overweight.equalsIgnoreCase("yes")){
					   premium=premium+(premium*0.01);//System.out.println("overweight"+ premium);
				   }
			   
			   
			   //habits
			   
			  
				   
				   if(smoking.equalsIgnoreCase("yes")){
					   premium=premium+(premium*0.03);//System.out.println("smoking"+ premium);
				   }
				   if(alcohol.equalsIgnoreCase("yes")){
					   premium=premium+(premium*0.03); //System.out.println("alcohol"+ premium);
				   }
				   if(drugs.equalsIgnoreCase("yes")){
					   premium=premium+(premium*0.03); //System.out.println("drugs"+ premium);
				   }
			   
			   if(daily_exercise.equalsIgnoreCase("yes")){
				   premium=(premium*0.97); //System.out.println("daily"+ premium);
			   }
			   
		   
		//System.out.println(premium);
		   
		output=(int)premium;
		   System.out.println("Health Insurance Premium for "+title+". "+name+": Rs."+output);
		   
		   
		   //System.out.println("Rollno:"+rollno+" name:"+name+" fee:"+fee);  
		   sc.close();  
		}
		catch(Exception e){
			System.out.println("Exception occurred :" +e);
		}

	}

}
